from app import app
from flask import render_template, url_for, redirect, flash
from EventMgrAPI.EventInformation import EventInformation
from EventMgrAPI.CreateUser import CreateUser

from forms import *

event_info = EventInformation()

@app.context_processor
def inject_user():
    return dict(event_info=event_info)


@app.route('/test')
def test():
	return render_template('test.html', title=event_info.event_name)
	
@app.route('/')
@app.route('/index')
def home():
	return render_template('home.html', title="Home")
	
@app.route('/about')
def about():
	return render_template('about.html', title='About')
	
@app.route('/register', methods=["GET", "POST"])
def register():
	form = RegistrationForm()
	
	if form.validate_on_submit():
		first_name = form.first_name.data
		last_name = form.last_name.data
		interests = form.interests.data
		
		nu = CreateUser(first_name, last_name, interests)
		result = nu.add()
		user_id = result['user_id']
		
		
		
		flash(f"You signed up successfully! Important: your ID is {user_id}")
		
		return redirect(url_for("home"))
		
		
	
	return render_template('register.html', title=f"Register for {event_info.event_name}", form=form)
	

	
