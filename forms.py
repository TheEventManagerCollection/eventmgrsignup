from flask_wtf import FlaskForm
from config import Config

from wtforms import StringField, BooleanField, SubmitField, SelectMultipleField, widgets
from wtforms.validators import DataRequired

current_config = Config()

class RegistrationForm(FlaskForm):
	first_name = StringField('First Name', validators=[DataRequired()])
	last_name = StringField('Last Name', validators=[DataRequired()])

	interests = SelectMultipleField("Interests", choices=current_config.get_interests_list(), 
										option_widget=widgets.CheckboxInput(), widget=widgets.ListWidget(prefix_label=False))

	submit = SubmitField("Register")
